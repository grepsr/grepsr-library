from setuptools import setup, find_packages

setup(name='grepsr',
      version='2.0.4',
      description='Grepsr common libraries',
      author='Subrat Basnet',
      author_email='subrat@grepsr.com',
      license='MIT',
      packages=find_packages(),
      scripts=["bin/test_config_depth"],
      install_requires=[
          'boto3==1.24.20', 'dnspython>=1.15.0', 'distconfig[zookeeper]==0.1.0',
          'pyyaml>=3.12', 'kazoo==2.8.0', 'grpcio==1.43.0', 'grpcio-tools==1.43.0',
          'ruamel.yaml==0.16.12'
      ],
      zip_safe=False)
