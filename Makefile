bump-version:
	# Checking `ver` variable, acceptable values: major, minor, patch
	[ "$(ver)" == "major" ] || [ "$(ver)" == "minor" ] \
		|| [ "$(ver)" == "patch" ] && bumpversion $(ver)
