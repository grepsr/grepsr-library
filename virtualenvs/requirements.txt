grpcio==1.43.0
grpcio-tools==1.43.0
boto3==1.23.09
dnspython>=1.15.0
distconfig[zookeeper]==0.1.0
pyyaml==3.12
kazoo==2.8.0
ruamel.yaml==0.16.12