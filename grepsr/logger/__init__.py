import logging
import os
import sys

_app_env = os.environ.get('APP_ENV', 'dev')
_logger = logging.getLogger(
    _app_env
)

def _init_logger():
    global _logger

    if _logger.hasHandlers():
        return

    _logger.propagate = False
    _log_level = os.environ.get('APP_LOG_LEVEL', None)

    if _log_level:
        _log_level = logging._nameToLevel.get(_log_level, logging.DEBUG)
    else:
        if _app_env == 'prd':
            _log_level = logging.INFO
        else:
            _log_level = logging.DEBUG

    _logger.setLevel(_log_level)

    _hn = logging.StreamHandler(sys.stderr)
    _formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    _hn.setFormatter(_formatter)

    # add handler to logger object
    _logger.addHandler(_hn)


def warn(msg, *args, **kwargs):
    global _logger
    msg = msg if msg else "N/A"

    # hide error when logger hostname can't be resolved
    try:
        _logger.warn(msg, exc_info=False, *args, **kwargs)
    except Exception:
        pass


def info(msg, *args, **kwargs):
    global _logger
    msg = msg if msg else "N/A"

    # hide error when logger hostname can't be resolved
    try:
        _logger.info(msg, exc_info=False, *args, **kwargs)
    except Exception:
        pass


def error(msg, *args, **kwargs):
    global _logger
    msg = msg if msg else "N/A"

    if type(msg) is str:
        exc_info = False
    else:
        exc_info = True

    # hide error when logger hostname can't be resolved
    try:
        _logger.error(msg, exc_info=exc_info, *args, **kwargs)
    except Exception:
        pass


def debug(msg, *args, **kwargs):

    global _logger
    msg = msg if msg else "N/A"
    # hide error when logger hostname can't be resolved
    try:
        _logger.debug(msg, exc_info=False, *args, **kwargs)
    except Exception:
        pass


def exception(msg, *args, **kwargs):
    global _logger
    msg = msg if msg else "N/A"

    if type(msg) is str:
        exc_info = False
    else:
        exc_info = True

    try:
        _logger.exception(msg, exc_info=exc_info, *args, **kwargs)
    except Exception:
        pass

# init the logger
_init_logger()