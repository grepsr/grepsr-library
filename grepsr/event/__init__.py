import boto3
import re
import json
import uuid
import base64
import grepsr.logger as log
from datetime import datetime
from .serializer.event_pb2 import Event as EventPayload


MESSAGE_TYPE_PROTOBUF = 'PROTOBUF'
MESSAGE_TYPE_JSON = 'JSON'


class Event:

    def __init__(self, aws_access_key_id=None,
                 aws_secret_access_key=None, aws_region=None, topic_arn=None, event_bus=None):

        params = dict()
        if aws_access_key_id and aws_secret_access_key:
            params['aws_access_key_id'] = aws_access_key_id
            params['aws_secret_access_key'] = aws_secret_access_key

        if aws_region:
            params['region_name'] = aws_region

        self.client = boto3.client('sns', **params)
        self.eb_client = boto3.client('events', **params)
        self.aggregate_id = str(uuid.uuid4())
        self.topic_arn = topic_arn
        self.event_bus = event_bus

    def emit(self, actor, event_type, message_type=MESSAGE_TYPE_PROTOBUF, data=None,
             ttl=0, source='', query_id='', team_id=None, additional_attributes=None,
             org_id=None, namespace=None):
        if not data:
            data = dict()

        replace_empty_string_values(data)

        event_category = self.get_event_category(event_type=event_type)
        if not event_category:
            log.warn("Ignoring event {} as it does not match the event naming pattern".format(event_type))
            return

        data_dict = data
        data = json.dumps(data)
        published_on = '{}Z'.format(datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3])

        if message_type == MESSAGE_TYPE_PROTOBUF:
            event_payload = EventPayload()
            event_payload.type = event_type
            event_payload.id = str(uuid.uuid4())
            event_payload.aggregate_id = self.aggregate_id
            event_payload.query_id = query_id
            event_payload.data = data
            event_payload.meta.actor = actor
            event_payload.meta.published_on = published_on
            event_payload.meta.source = source
            event_payload.ttl = ttl

            serialized_event_payload = event_payload.SerializeToString()
            serialized_event_payload = base64.b64encode(serialized_event_payload)

        elif message_type == MESSAGE_TYPE_JSON:
            meta = {
                'actor': actor,
                'published_on': published_on,
                'source': source,
                'resources':{}
            }
            if team_id:
                meta['team_id'] = team_id
            if org_id:
                meta['org_id'] = org_id
            if namespace:
                meta['ns'] = namespace

            if 'report_id' in data_dict:
                meta['resources']['report_id'] = data_dict['report_id']

            if 'project_id' in data_dict:
                meta['resources']['project_id'] = data_dict['project_id']

            if 'schedule_id' in data_dict:
                meta['resources']['schedule_id'] = data_dict['schedule_id']
            
            if 'history_id' in data_dict:
                meta['resources']['history_id'] = data_dict['history_id']

            if 'extractor_id' in data_dict:
                meta['resources']['report_id'] = data_dict['extractor_id']
            
            event_payload = {
                'type': event_type,
                'id': str(uuid.uuid4()),
                'aggregate_id': self.aggregate_id,
                'query_id': query_id,
                'data': data,
                'meta': meta,
                'ttl': ttl
            }

            serialized_event_payload = json.dumps(event_payload)
            serialized_event_payload = base64.b64encode(serialized_event_payload.encode('utf-8'))

        else:
            log.error('Unsupported message type: {}'.format(message_type))
            return

        message_attributes = {
            'event_type': {
                'DataType': 'String',
                'StringValue': event_type
            },
            'event_category': {
                'DataType': 'String',
                'StringValue': event_category
            },
            'message_type': {
                'DataType': 'String',
                'StringValue': message_type
            }
        }

        if additional_attributes:
            for key in additional_attributes:
                message_attributes[key] = {
                    'DataType': 'String',
                    'StringValue': additional_attributes[key]
                }

        # don't send anything to SNS
        try:
           response = self.client.publish(
               TopicArn=self.topic_arn,
               MessageStructure='string',
               Message=serialized_event_payload.decode('utf-8'),
               MessageAttributes=message_attributes
           )
        except Exception as e:
           log.exception(str(e))
           response = {}

        # Send to eventbridge.
        if True:
            meta = {
                'actor': actor,
                'published_on': published_on,
                'source': source,
                'resources':{}
            }
            if team_id:
                meta['team_id'] = team_id
            if org_id:
                meta['org_id'] = org_id
            if namespace:
                meta['ns'] = namespace
            event_payload = {
                'type': event_type,
                'id': str(uuid.uuid4()),
                'aggregate_id': self.aggregate_id,
                'query_id': query_id,
                'data': data,
                'meta': meta,
                'ttl': ttl
            }

            # Add these info to outer level object (for eventbridge only)
            if team_id:
                event_payload['team_id'] = team_id
            if namespace:
                event_payload['ns'] = namespace

            if 'report_id' in data_dict:
                meta['resources']['report_id'] = data_dict['report_id']

            if 'project_id' in data_dict:
                meta['resources']['project_id'] = data_dict['project_id']

            if 'schedule_id' in data_dict:
                meta['resources']['schedule_id'] = data_dict['schedule_id']
            
            if 'history_id' in data_dict:
                meta['resources']['history_id'] = data_dict['history_id']

            if 'extractor_id' in data_dict:
                meta['resources']['extractor_id'] = data_dict['extractor_id']

            if 'org_id' in data_dict:
                event_payload['org_id'] = data_dict['org_id']

            serialized_event_payload = json.dumps(event_payload)

            try:
                event_obj = {
                    'Time': datetime.utcnow(),
                    'Source': source,
                    'DetailType': event_type,
                    'Detail': serialized_event_payload,
                    'EventBusName': self.event_bus
                }
                response = self.eb_client.put_events(Entries=[event_obj])
            except Exception as e:
                response = {}
                log.exception(str(e))

        return response

    @staticmethod
    def get_event_category(event_type):
        ret = re.search(r'^([A-Z][a-z]+)([A-Z][a-z]+)?[A-Z][a-z]+edEvent+$', event_type)
        if ret:
            return ret.group(1)
        return ''


def replace_empty_string_values(d, replace_with=None):
    for k, v in d.items():
        if v and type(v) is dict:
            replace_empty_string_values(v)
        elif v == '':
            d[k] = replace_with
