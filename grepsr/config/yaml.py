import os
import grepsr.logger as log
from ruamel.yaml import YAML
from . import MAX_CONFIG_DEPTH, APP_ENV

config = None


class DotAccessibleDict(object):
    def __init__(self, data):
        self._data = data

    def __getitem__(self, name):
        val = self._data
        for key in name.split('.'):
            val = val[key]
        return val


def init():
    global config
    config_file = os.path.join(os.getcwd(), 'config/{}.yml'.format(APP_ENV))
    if not os.path.isfile(config_file):
        # try checking one directory up
        config_file = os.path.abspath(os.path.join(os.getcwd(), '../config/{}.yml'.format(APP_ENV)))
    with open(config_file, 'r') as stream:
        yaml = YAML()
        config = DotAccessibleDict(yaml.load(stream))
    return config


def get_config(key, default=None):
    global config

    # Give priority to environment variables
    ret = get_from_env(key=key)
    if not ret:
        try:
            ret = config[key]
        except KeyError:
            ret = default

    if not ret:
        ret = default

    return ret


def get_from_env(key):
    # Example key: a.b.c, environment variable: CFG_A_B_C
    key = 'CFG_{}'.format('_'.join(list(map(lambda k: k.upper(), key.split('.')))))
    ret = os.environ.get(key)
    return ret


def cfg(key, default=None, log_key=False):
    global config

    if not config:
        log.error('Config has not been initialized yet')
        return

    count = key.count('.') + 1

    if count > MAX_CONFIG_DEPTH:
        log.warn(
            'Simplify config setting "{}" by '
            'limiting depth to {} levels from current depth of {}'.format(
                key,
                MAX_CONFIG_DEPTH,
                count
            )
        )

    ret = get_config(key, default=default) if config else None
    if log_key:
        log.info('Getting config; key: {}, value: {}, default: {}'.format(key, ret, default))

    return ret
