import os
import grepsr.logger as log
from kazoo import client
from distconfig import Proxy
from yaml import load
from . import MAX_CONFIG_DEPTH

APP_ENV = os.environ.get('APP_ENV', None)


config = None
proxy = None
state = None


def is_production():
    return True if APP_ENV == 'prd' else False


def state_listener(kazoo_state):
    global state
    state = kazoo_state


def init(zk_hosts, root):

    global config, proxy
    cl = client.KazooClient(hosts=zk_hosts)
    cl.add_listener(state_listener)
    cl.start()
    proxy = Proxy.configure(
        'distconfig.backends.zookeeper.ZooKeeperBackend',
        client=cl,
        parser=load
    )
    config = proxy.get_config(root) if proxy else None
    return cl


def cfg(key, default=None, root=None, log_key=False):
    global config, proxy, state
    key_original = key
    key = key_original.replace('.', '/').strip()
    count = key.count('/') + 1

    if count > MAX_CONFIG_DEPTH:
        log.warn(
            "Simplify config setting '{}' by "
            "limiting depth to {} levels from current depth of {}".format(
                key_original,
                MAX_CONFIG_DEPTH, count
            )
        )

    if state not in [client.KazooState.LOST, client.KazooState.SUSPENDED]:
        # Refresh config
        config = proxy.get_config(root) if root else config

    ret = config.get(path=key, default=default) if config else None

    if log_key:
        log.info("Getting config; key: {}, value: {}, default: {}, root: {}".
                 format(
                     key_original, ret, default, root)
                 )

    return ret
