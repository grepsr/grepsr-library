import time
from zooconfig import init, cfg


def main():
    counter = 0
    # Initialize
    cl = init(zk_hosts='192.168.88.5:2181,192.168.88.5:2181', root='/altos/dev/test-service')
    while True:
        counter += 1
        if counter % 2 == 0:
            cl.stop()
        else:
            cl.start()
        print(cfg(key='mysql.bl', root='/altos/dev/test-service'))
        time.sleep(5)


if __name__ == '__main__':
    main()
